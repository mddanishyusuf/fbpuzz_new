app.controller('pageController',['$scope','$http','$stateParams','$sce','$analytics','ngMeta',function($scope, $http, $stateParams, $sce,$analytics,ngMeta){
    var trackUrl = '/p/' + $stateParams.page_id    
    document.getElementsByTagName("body")[0].setAttribute("data-status", "notready");
    $analytics.pageTrack(trackUrl);
    $scope.show_more_load = false;
    $scope.show_bottom = false;
    url4 = "https://api.fbpuzz.com/api/getpageInfo/" + $stateParams.page_id
    $http.get(url4).then(function(response){
        $scope.page_info = response.data    
        ngMeta.setTitle(response.data.name + ' Videos');
        ngMeta.setTag('description', response.data.description);
        ngMeta.setTag('og:image', response.data.cover);
    },function(response){
    // failure call back
    })
    var url = "https://api.fbpuzz.com/api/get/" + $stateParams.page_id
    var result = [] 
    var l = 2
        $http({
            method: "GET",
            url: url
        }).success(function(data) {
            angular.forEach(data, function(value) {
            result.push(value)
        })            
        }).error(function() {
            console.log('Error');
        }).finally(function() {
            $scope.show_more_load = true;
            $scope.show_bottom = true;
            $scope.status = 'ready';
            document.getElementsByTagName("body")[0].setAttribute("data-status", "ready");
        })
        $scope.page_res = result

$scope.disable_scroll = true

    $scope.loadMore = function() {
    $scope.show_more_load = false;
        $scope.disable_scroll = true
        var url = "https://api.fbpuzz.com/api/get/" + $stateParams.page_id + "/" + l
        $http({
            method: "GET",
            url: url,
            cache: false
        }).success(function(data) {          
            angular.forEach(data, function(value) {
            result.push(value)
        })
        }).error(function() {
            console.log("Error");
        }).finally(function() {
            $scope.load = true
            $scope.disable_scroll = false
        })
        $scope.page_res = result 
        l = l + 1;
    };

$scope.return_time = function(tm){
    return moment.unix(tm).fromNow()
}
$scope.return_length = function(tm){
    d = Number(tm);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + ":" : "";
    var mDisplay = m > 0 ? m + ":" : "00:";
    var sDisplay = s > 0 ? (s>9 ? s : "0"+s) : "00";
    return hDisplay + mDisplay + sDisplay; 
}
}])

