app.controller('sideNavbar',function($http, $scope, $state, ezfb, $window, $location, $q){
    document.getElementsByTagName("body")[0].setAttribute("data-status", "notready");

  updateMe();
  
  updateLoginStatus()
  .then(updateApiCall);

  /**
   * Subscribe to 'auth.statusChange' event to response to login/logout
   */
  ezfb.Event.subscribe('auth.statusChange', function (statusRes) {
    $scope.loginStatus = statusRes;

    updateMe();
    updateApiCall();
  });

  $scope.login = function () {
    /**
     * Calling FB.login with required permissions specified
     * https://developers.facebook.com/docs/reference/javascript/FB.login/v2.0
     */
    ezfb.login(null, {scope: 'email,user_likes'});

    /**
     * In the case you need to use the callback
     *
     * ezfb.login(function (res) {
     *   // Executes 1
     * }, {scope: 'email,user_likes'})
     * .then(function (res) {
     *   // Executes 2
     * })
     *
     * Note that the `res` result is shared.
     * Changing the `res` in 1 will also change the one in 2
     */
  };

  $scope.logout = function () {
    /**
     * Calling FB.logout
     * https://developers.facebook.com/docs/reference/javascript/FB.logout
     */
    ezfb.logout();

    /**
     * In the case you need to use the callback
     *
     * ezfb.logout(function (res) {
     *   // Executes 1
     * })
     * .then(function (res) {
     *   // Executes 2
     * })
     */
  };


  
  /**
   * Update api('/me') result
   */
  function updateMe () {
    ezfb.getLoginStatus()
    .then(function (res) {
      // res: FB.getLoginStatus response
      // https://developers.facebook.com/docs/reference/javascript/FB.getLoginStatus
      return ezfb.api('/me');
    })
    .then(function (me) {
      // me: FB.api('/me') response
      // https://developers.facebook.com/docs/javascript/reference/FB.api
      $scope.me = me;
    });
  }
  
  /**
   * Update loginStatus result
   */
  function updateLoginStatus () {
    return ezfb.getLoginStatus()
      .then(function (res) {
        // res: FB.getLoginStatus response
        // https://developers.facebook.com/docs/reference/javascript/FB.getLoginStatus
        $scope.loginStatus = res;
      });
  }

  /**
   * Update demostration api calls result
   */
  function updateApiCall () {
    return $q.all([
        ezfb.api('/me'),
        ezfb.api('/me/likes')
      ])
      .then(function (resList) {
        // Runs after both api calls are done
        // resList[0]: FB.api('/me') response
        // resList[1]: FB.api('/me/likes') response
        $scope.apiRes = resList;
      });

  }


String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}
var ex_cat = ['COMEDY','ENTERTAINMENT','LIFESTYLE','MUSIC','NEWS','SCIENCE','SPORTS','TECHNOLOGY']
var show_cat = []
var cat_drop_down = []
function capitalize_string(str){
    var s = str.toLowerCase().split("_")
    for(var i = 0; i < s.length; i++) {
      var letters = s[i].split('');
      letters[0] = letters[0].toUpperCase();
      s[i] = letters.join('');
    }
  return s.join('-')
}
$http({
    method: "GET",
    url: 'https://api.fbpuzz.com/api/getPagesByCategories'
}).success(function(data) { 
    function isInArray(value, array) {
      return array.indexOf(value) > -1;
    }
    angular.forEach(data, function(value) {
    if(isInArray(value.content_category, ex_cat)){
        var c = capitalize_string(value.content_category)
        show_cat.push({'category': c, 'content_category':value.content_category})
    }else{
        var c = capitalize_string(value.content_category)
        cat_drop_down.push({'category': c, 'content_category':value.content_category})
    }  
    }) 
        
    $scope.category_down = cat_drop_down
    $scope.category_show = show_cat
}).error(function() {
  console.log("Error");
}).finally(function() {
    
    document.getElementsByTagName("body")[0].setAttribute("data-status", "ready");
})

$http({
        method: "GET",
        url: 'https://api.fbpuzz.com/api/getPages/'
    }).success(function(data) {  
        $scope.page_name = data
    }).error(function() {
        console.log("Error");
    }).finally(function() {
    }) 

})