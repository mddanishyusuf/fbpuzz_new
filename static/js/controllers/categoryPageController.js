app.controller('categoryPageController',["$scope","$http","$stateParams","$state","$analytics","ngMeta",function($scope, $http, $stateParams, $state, $analytics,ngMeta){
  var trackUrl = '/category/' + $stateParams.category
  document.getElementsByTagName("body")[0].setAttribute("data-status", "notready");
  $analytics.pageTrack(trackUrl);
  $scope.show_more_load = false;
  $scope.show_bottom = false;
  var url = "https://api.fbpuzz.com/api/getposts/" + $stateParams.category
  var result = [] 

  var cat_title = $stateParams.category + ' Videos'
  var cat_description = 'facebook '+$stateParams.category + ' Videos'

  ngMeta.setTitle(cat_title);
  ngMeta.setTag('description', cat_description);
  var l = 2
	$http({
        method: "GET",
        url: url,
        cache: false
    }).success(function(data) {
        angular.forEach(data, function(value) {
	        result.push(value)
	      })
    }).error(function() {
        console.log('Error');
    }).finally(function() {
            $scope.show_more_load = true;
            $scope.show_bottom = true;
            // $scope.status = 'ready';
            document.getElementsByTagName("body")[0].setAttribute("data-status", "ready");
    })
    $scope.page_res = result

$scope.disable_scroll = true

$scope.loadMore = function() {
    $scope.disable_scroll = true
    var url = "https://api.fbpuzz.com/api/getposts/" + $stateParams.category + "/" + l
    $http({
        method: "GET",
        url: url,
        cache: false
        }).success(function(data) {          
            angular.forEach(data, function(value) {
              result.push(value)
            })
        }).error(function() {
            console.log("Error");
        }).finally(function() {
            $scope.disable_scroll = false
        })
    $scope.page_res = result 
    l = l + 1;
  };
$scope.return_time = function(tm){
  return moment.unix(tm).fromNow()
}
  
$scope.return_length = function(tm){
  d = Number(tm);
  var h = Math.floor(d / 3600);
  var m = Math.floor(d % 3600 / 60);
  var s = Math.floor(d % 3600 % 60);

  var hDisplay = h > 0 ? h + ":" : "";
  var mDisplay = m > 0 ? m + ":" : "00:";
  var sDisplay = s > 0 ? (s>9 ? s : "0"+s) : "00";
  return hDisplay + mDisplay + sDisplay; 
}
}])