app.controller('homeController',function($scope, $http, $rootScope, ngMeta){
    ngMeta.setTitle('FBpuzz.com | Watch and Download facebook videos');
    ngMeta.setTag('description', 'FBpuzz.com is collections of facebook videos of pranks, funny, comedy, entertainment, food, health etc. online or stream right to your PC, Mac, mobile, tablet and more.');
    ngMeta.setTag('og:image', 'https://www.fbpuzz.com/img/facebook-cover.jpg');
    ngMeta.setTag('keywords', 'watch,facebook,videos,download,videos,pranks,funny,comedy,entertainment,fun,funny,lol,wtf,omg,fail,cosplay,forever alone,cute');
    document.getElementsByTagName("body")[0].setAttribute("data-status", "notready");
    $scope.date = new Date();
    $scope.show_more_load = false;
    var l = 2
    var result = []

        $http({
            method: "GET",
            url: "https://api.fbpuzz.com/data.js"
        }).success(function(data) {
            angular.forEach(data, function(value) {
            result.push(value)
        })
        }).error(function() {
            console.log('Error');
        }).finally(function() {
            $scope.show_more_load = true;
            $rootScope.show_footer = false;
            document.getElementsByTagName("body")[0].setAttribute("data-status", "ready");
        })
        $scope.result = result

$scope.disable_scroll = true

$scope.loadMore = function() {
    $scope.show_more_load = false;
    $scope.disable_scroll = true
    var url = "https://api.fbpuzz.com/api/getposts/" + l;
        $http({
            method: "GET",
            url: url
        }).success(function(data) {            
            angular.forEach(data, function(value) {
            result.push(value)
        })
        }).error(function() {
            console.log("Error");
        }).finally(function() {
            $scope.disable_scroll = false
        })
        $scope.result = result 
        l = l + 1;
};

$scope.return_time = function(tm){
    return moment.unix(tm).fromNow()
}
$scope.return_length = function(tm){
    d = Number(tm);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + ":" : "";
    var mDisplay = m > 0 ? m + ":" : "00:";
    var sDisplay = s > 0 ? (s>9 ? s : "0"+s) : "00";
    return hDisplay + mDisplay + sDisplay; 
}
})