app.controller('videoPageController',['$scope','$http','$stateParams','$sce','$analytics','ngMeta',function($scope, $http, $stateParams, $sce, $analytics, ngMeta){
    document.getElementsByTagName("body")[0].setAttribute("data-status", "notready");
    var trackUrl = '/v/' + $stateParams.page_id+'/'+$stateParams.post_id
    $analytics.pageTrack(trackUrl);
    var url = 'https://api.fbpuzz.com/api/video/'+$stateParams.page_id+'/'+$stateParams.post_id
    $scope.page_name = $stateParams.page_id
    $scope.post_id = $stateParams.post_id
    $scope.show_more_load = false;
    $scope.show_bottom = false;
    $scope.show_video_box = false;
    var l = 2

    url4 = "https://api.fbpuzz.com/api/getpageInfo/" + $stateParams.page_id
    $http.get(url4).then(function(response){
        $scope.page_info = response.data
    },function(response){
    // failure call back
    })

    // Opera 8.0+
    var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

    // Firefox 1.0+
    var isFirefox = typeof InstallTrigger !== 'undefined';

    // Safari 3.0+ "[object HTMLElementConstructor]" 
    var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification);

    // Internet Explorer 6-11
    var isIE = /*@cc_on!@*/false || !!document.documentMode;

    // Edge 20+
    var isEdge = !isIE && !!window.StyleMedia;

    // Chrome 1+
    var isChrome = !!window.chrome && !!window.chrome.webstore;

    // Blink engine detection
    var isBlink = (isChrome || isOpera) && !!window.CSS;

    $http.get(url).then(function(response){
        $scope.vid = response.data
        var video_data = response.data
        var category_name = video_data.content_category
        $scope.description = video_data.description
        $scope.short_description = video_data.description ? video_data.description.substr(0, 50) : video_data.description;
        $scope.vid_title = video_data.title ? video_data.title : ($scope.page_name ? $scope.page_name + ' Video' : $scope.short_description)
        $analytics.eventTrack('Click', {  category: category_name, label: $scope.vid_title });

        $scope.video_image = "https://api.fbpuzz.com/api/images/"+$stateParams.page_id+"/"+$stateParams.post_id+".jpg"
        ngMeta.setTitle($scope.vid_title);
        ngMeta.setTag('og:image', $scope.video_image);
        ngMeta.setTag('description', $scope.description);

    // var poster = response.data.format[1].picture ? response.data.format[1].picture : ( response.data.format[0].picture ? response.data.format[0].picture : ( response.data.format[0].picture ? response.data.format[0].picture : response.data.picture))
        
    if(isFirefox || isSafari || isEdge){
        $scope.video_link = "http://dl.fbpuzz.com/dl/"+ $scope.page_name + "/" + response.data.id
    }else{
        $scope.video_link = response.data.source
    }

    $scope.config = {
        preload: "none",
        sources: [
            {src: $sce.trustAsResourceUrl(response.data.source), type: "video/mp4"}
        ],
        theme: "static/css/videogular.min.css",
        plugins: {
            // poster: poster,
            controls: {
                autoHide: true,
                autoHideTime: 3000
            },         
            // analytics: {
            //     category: "Videogular",
            //     label: "Main",
            //     events: {
            //         ready: true,
            //         play: true,
            //         pause: true,
            //         stop: true,
            //         complete: true,
            //         progress: 10
            //     }
            // }
        }
    };
       }, 
       function(response){
         // failure call back
         console.log('error')
            
       }
    ).finally(function() {        
        // $rootScope.status = 'ready';
        // document.getElementsByTagName("body")[0].setAttribute("data-status", "ready");
    });

var url2 = "https://api.fbpuzz.com/api/get/" + $stateParams.page_id + '?count=4'
var result = []
    $http.get(url2).then(function(response){
        angular.forEach(response.data, function(value) {
            result.push(value)
          })
        var url3 = "https://api.fbpuzz.com/api/getposts/" + response.data[0].content_category
            $http.get(url3).then(function(response){
               angular.forEach(response.data, function(value) {
                    result.push(value)
                  })
            },function(response){
                // failure call back
            }).finally(function(){
                document.getElementsByTagName("body")[0].setAttribute("data-status", "ready"); 
            })
    },function(response){
        // failure call back
    }).finally(function() {
        $scope.show_more_load = true;
        $scope.show_bottom = true;
        $scope.show_video_box = true;
    })
$scope.related_data = result
$scope.disable_scroll = true
$scope.loadMore = function(cat) {
    var url3 = "https://api.fbpuzz.com/api/getposts/" + cat + "/" + l 
    $scope.disable_scroll = true
    var url = url3
    $http({
        method: "GET",
        url: url,
        cache: false
        }).success(function(data) {          
            angular.forEach(data, function(value) {
              result.push(value)
            })
        }).error(function() {
            console.log("Error");
        }).finally(function() {
            $scope.disable_scroll = false
        })
    $scope.related_data = result 
    l = l + 1;
  };
$scope.return_time = function(tm){
    return moment.unix(tm).fromNow()
}
$scope.return_length = function(tm){
    d = Number(tm);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + ":" : "";
    var mDisplay = m > 0 ? m + ":" : "00:";
    var sDisplay = s > 0 ? (s>9 ? s : "0"+s) : "00";
    return hDisplay + mDisplay + sDisplay; 
}
}])