'use strict';
var app = angular.module('FBpuzz',
  [
    "ui.router",
    "angularCSS",
    "ngSanitize",
    "angulartics",
    "angulartics.google.analytics",
    "infinite-scroll",
    "com.2fdevs.videogular",
    "com.2fdevs.videogular.plugins.controls",
    "com.2fdevs.videogular.plugins.overlayplay",
    "com.2fdevs.videogular.plugins.poster",
    "com.2fdevs.videogular.plugins.buffering",
    "angular-loading-bar",
    "ngMeta",
    "ezfb"
  ]
);
app.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.parentSelector = '#loading-bar-container';
    cfpLoadingBarProvider.spinnerTemplate = '<div class="load_more_text"><span class="fa fa-spinner">Loading...</div>';
  }]);

app.run(function(ngMeta) {
    ngMeta.init();
});

app.config(function (ezfbProvider) {
  ezfbProvider.setInitParams({
    // This is my FB app id for plunker demo app
    appId: '1643387909294081',

    // Module default is `v2.6`.
    // If you want to use Facebook platform `v2.3`, you'll have to add the following parameter.
    // https://developers.facebook.com/docs/javascript/reference/FB.init
    // version: 'v2.3'
  });  
});

app.config([  
    '$locationProvider',
    function($locationProvider) {
        $locationProvider.hashPrefix('!');
    }
]);

app.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
    
    $urlRouterProvider.otherwise('/home');
    
    $stateProvider
        
        .state('home', {
            url: '/home',
            templateUrl: 'views/home.html',
            controller: 'homeController',
            css: {
                href: 'static/css/home-style.css',
                bustCache: true
              }
        })
        
        .state('video_page', {
            url: '/v/:page_id/:post_id',
            templateUrl: 'views/video_page.html',
            controller: 'videoPageController',
            css: [
                {
                    href: 'static/css/home-style.css',
                    bustCache: true
                },
                {
                    href: 'static/css/single-video.css',
                    bustCache: true
                }
            ]
        })
        
        .state('all_vid_of_page', {
            url: '/p/:page_id',
            templateUrl: 'views/single_page.html',
            controller: 'pageController',
            css: {
                href: 'static/css/home-style.css',
                bustCache: true
              }
        })
        
        .state('vid_by_category', {
            url: '/category/:category',
            templateUrl: 'views/category_page.html',
            controller: 'categoryPageController',
            css: {
                href: 'static/css/home-style.css',
                bustCache: true
              }
        })
        
        // .state('single_video', {
        //     url: '/video/:page_name/:post_id',
        //     templateUrl: 'views/single_video.html',
        //     controller: 'singleVideoController'
        // })
        
        .state('feedback', {
            url: '/feedback',
            templateUrl: 'views/feedback_page.html'
        })
        
        .state('privacy', {
            url: '/privacy',
            templateUrl: 'views/privacy_page.html'
        })
        
        .state('terms', {
            url: '/terms',
            templateUrl: 'views/terms_page.html'
        })
        
        .state('404', {
            url: '/404',
            templateUrl: 'views/page_404.html'
        });

        // $locationProvider.html5Mode(true);
      
})